CREATE TABLE users
(
  userid VARCHAR(32) NOT NULL,
  password VARCHAR(256) NOT NULL,
  mail VARCHAR(64) NOT NULL,
  active bool NOT NULL,
  PRIMARY KEY (userid),
  UNIQUE (mail)
);

CREATE TABLE class
(
  classid SERIAL NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (classid)
);

CREATE TABLE relation
(
  userid VARCHAR(32) NOT NULL,
  classid SERIAL NOT NULL,
  FOREIGN KEY (userid) REFERENCES users(userid),
  FOREIGN KEY (classid) REFERENCES class(classid)
);

CREATE TABLE confirm
(
  passcode VARCHAR(6) NOT NULL,
  userid VARCHAR(32) NOT NULL,
  FOREIGN KEY (userid) REFERENCES users(userid)
);

CREATE TABLE dataset
(
  datasetid SERIAL NOT NULL,
  name VARCHAR(32) NOT NULL,
  userid VARCHAR(32) NOT NULL,
  FOREIGN KEY (userid) REFERENCES users(userid)
);

CREATE TABLE train
(
  trainid VARCHAR(64) NOT NULL,
  options jsonb NOT NULL,
  userid VARCHAR(32) NOT NULL,
  PRIMARY KEY (trainid),
  FOREIGN KEY (userid) REFERENCES users(userid)
);