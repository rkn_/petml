# -*- coding: utf-8 -*-
import os
from flask import Flask, request
import psycopg2
import json
from backend.tools.S3connection import get_dataset
from backend.ML.train import setup, do_train
from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer

app = Flask(__name__)
@app.route('/', methods=['POST, GET'])
def endpoint():
    print(vars(request))

def connect():
    return psycopg2.connect("host=petml_db dbname=petml user=lkeix password=Gakuuo0309 sslmode=disable")

# userid から 直近のtrainidを引き出してくる
def get_trainid(userid):
    conn = connect()
    cur = conn.cursor()
    cur.execute("select trainid, options from train where userid=%s", userid)
    (trainid, options) = cur.fetchone()
    cur.close()
    conn.close()
    app.logger.debug(trainid)
    app.logger.debug(options)

@app.route('/api/storeTrain', methods=['POST'])
def trainpoint():
    # json 文字列を取得
    req = request.form.get('data')
    # decode
    obj = json.loads(req)
    # s3からデータを取得する
    userid, num_classes = get_dataset(obj['ID'], obj['train'], obj['test'], app.logger)
    app.logger.debug(num_classes)
    app.logger.debug(obj)
    nomalization = False
    whitening = False
    height_flip = False
    width_flip = False
    Shift = False
    if '正規化' in obj['selectpreprocess']:
      nomalization = True
    if '白色化' in obj['selectpreprocess']:
      whitening = True
    if '横反転' in obj['selectpreprocess']:
      width_flip = True
    if '縦反転' in obj['selectpreprocess']:
      height_flip = True
    if '分割' in obj['selectpreprocess']:
      Shift = True
    # 指定された処理を施して学習を開始
    params = setup(num_classes, obj['batch'], obj['epoch'], obj['steps'], nomalization, whitening, width_flip, height_flip, Shift, 0.0001, obj['loss'], obj['pretrained'], obj['optimizer'], userid)
    do_train(userid, params)
    return ''

# websocketでlossの値を返すAPIを定義 (logs/loss.csvに入っている)
@app.route('/api/getloss', methods=['POST'])
def getloss():
  res = ''
  if os.path.exists('./logs/loss.csv'):
    with open('./logs/loss.csv') as f:
      res = json.dumps(f.read())
  return res
  # if request.environ.get('wsgi.websocket'):

def application():
  app.debug = True
  host = '0.0.0.0'
  port = 8000
  host_port = (host, port)
  server = WSGIServer(
    host_port,
    app,
    handler_class=WebSocketHandler
  )
  server.serve_forever()

#if __name__ == '__main__':
#  application()
  # app.run(host='0.0.0.0', port=8000, debug=True)