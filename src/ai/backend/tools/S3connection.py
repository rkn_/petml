import boto3
import os

# 文字列pathが存在するか確認するなかったら作成する．
def dir_exist(path):
  if not os.path.exists(path):
    os.makedirs(path)

# ユーザIDから指定されたデータセットを取得する。
def get_dataset(userid, train_dataset, test_dataset, debugger):
  s3 = boto3.resource('s3', aws_access_key_id="AKIASMP2OFIRPVWKDLVE", aws_secret_access_key="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", region_name='us-east-1')
  bucket = s3.Bucket('petml')
  classes = bucket.meta.client.list_objects_v2(Bucket=bucket.name, Prefix='{}/{}/'.format(userid, train_dataset)).get('Contents')
  testclasses = bucket.meta.client.list_objects_v2(Bucket=bucket.name, Prefix='{}/{}/'.format(userid, test_dataset)).get('Contents')
  num_classes = 0
  last_class = ''
  for c in classes:
    classname = c.get('Key')
    debugger.debug(classname)
    base, _ = os.path.split(classname)
    base = os.path.basename(base)
    if last_class != base:
      num_classes += 1
      last_class = base
    files = bucket.meta.client.list_objects_v2(Bucket=bucket.name, Prefix='{}/'.format(os.path.dirname(classname))).get('Contents')
    cnt = 0
    for f in files:
      f = f.get('Key')
      out = '{}/../ML/datasets/{}/train/{}'.format(os.path.dirname(os.path.abspath(__file__)), userid, base)
      dir_exist(out)
      bucket.download_file('{}'.format(f), '{}/{}.jpg'.format(out, cnt))
      cnt += 1
  for c in testclasses:
    classname = c.get('Key')
    base, _ = os.path.split(classname)
    base = os.path.basename(base)
    files = bucket.meta.client.list_objects_v2(Bucket=bucket.name, Prefix='{}/'.format(os.path.dirname(classname))).get('Contents')
    cnt = 0
    for f in files:
      f = f.get('Key')
      out = '{}/../ML/datasets/{}/test/{}'.format(os.path.dirname(os.path.abspath(__file__)), userid, base)
      dir_exist(out)
      bucket.download_file('{}'.format(f), '{}/{}.jpg'.format(out, cnt))
      cnt += 1
  return userid, num_classes