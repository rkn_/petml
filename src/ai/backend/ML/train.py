# -*- coding: utf-8 -*-
import warnings
import keras
from keras.applications import VGG16, VGG19, Xception, InceptionV3, ResNet50, DenseNet201, DenseNet169, DenseNet121, InceptionResNetV2, NASNetLarge, NASNetMobile, MobileNet, MobileNetV2
from keras.optimizers import RMSprop, SGD, Adagrad, Adadelta, Adam, Adamax, Nadam
from keras.models import Model
from keras.layers import Activation, Dropout, Flatten, Dense, GlobalAveragePooling2D, Convolution2D, MaxPooling2D, Input
from keras.preprocessing.image import ImageDataGenerator
import glob
import os
import numpy as np
import datetime
import json
import csv

class Params:
    def __init__(self, num_class, batch, epoch, steps, normalization, whitening, horizontal_flip, vertical_flip, shift, lr, lossfunc, pretrained, optimizer, trainid):
        self.num_class = num_class
        self.batch = batch
        self.epoch = epoch
        self.steps = steps
        self.normalization = normalization
        self.whitening = whitening
        self.horizontal_flip = horizontal_flip
        self.vertical_flip = vertical_flip
        self.lr = lr
        self.width_shift = 0.
        self.height_shift = 0.
        self.trainid = trainid
        self.pretrained = pretrained
        self.lossfunc = lossfunc
        self.optimizer = optimizer
        if shift:
            self.width_shift = 0.2
            self.height_shift = 0.2

class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
        with open("./logs/loss.csv", "w", encoding="utf-8") as f:
            writer = csv.writer(f, lineterminator="\n")
            writer.writerows([self.losses])

def dir_exist(path):
  if not os.path.exists(path):
    os.makedirs(path)

def setup(num_classes, batch, epoch, steps, normalization, whitening, horizontal_flip, vertical_flip, shift, lr, lossfunc, pretrained, optimizer, trainid):
    return Params(num_classes, batch, epoch, steps, normalization, whitening, horizontal_flip, vertical_flip, shift, lr, lossfunc, pretrained, optimizer, trainid)


def train_data_loader(userid, args):
    train_datagen = ImageDataGenerator(
        rescale=1.0 / 255,
        featurewise_std_normalization=args.normalization,
        samplewise_std_normalization=args.normalization,
        zca_whitening=args.whitening,
        width_shift_range=args.width_shift,
        height_shift_range=args.height_shift,
        horizontal_flip=args.horizontal_flip,
        vertical_flip=args.vertical_flip
    )
    target = '{}/datasets/{}/train'.format(os.path.dirname(os.path.abspath(__file__)), args.trainid)
    labels = glob.glob('{}/*'.format(target))
    train_label = []
    for label in labels:
        train_label.append(os.path.basename(label))
    f = open('{}/{}_{}_label.json'.format(target, userid, datetime.datetime.today().strftime("%Y-%m-%d-%H-%M-%S")), 'w')
    json.dump(train_label, f, indent=4)
    train_generator = train_datagen.flow_from_directory(
        target,
        target_size=(224,224),
        batch_size=args.batch,
        class_mode='categorical',
        shuffle=True
    )
    return train_generator

def test_data_loader(args):
    test_datagen = ImageDataGenerator(rescale=1.0 / 255)
    target = '{}/datasets/{}/train'.format(os.path.dirname(os.path.abspath(__file__)), args.trainid)
    test_generator = test_datagen.flow_from_directory(
        target,
        target_size=(224,224),
        batch_size=args.batch,
        class_mode='categorical',
        shuffle=True
    )
    return test_generator

def do_train(userid, args):
    history = LossHistory()
    # tb_cb = keras.callbacks.TensorBoard(log_dir="./logs", histogram_freq=1)
    warnings.simplefilter('ignore')
    # データセットの読み込み
    train_data = train_data_loader(userid, args)
    test_data = test_data_loader(args)
    # load pretrained model
    model = select_pretrained(args)
    x = model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    predictions = Dense(args.num_class, activation='softmax')(x)
    # 15層までを凍結
    for layer in model.layers[:15]:
        layer.trainable=False
    # 学習モデルの生成
    model = Model(input=model.input, outputs=predictions)
    # 最適化関数の定義
    model.compile(loss=args.lossfunc, optimizer=select_optimizer(args), metrics=['accuracy'])
    model.summary()
    # 学習
    model.fit_generator(train_data, steps_per_epoch=args.steps, epochs=args.epoch, validation_data=test_data, validation_steps=args.batch, verbose=1, callbacks=[history])
    # 重みの保存
    # ユーザ名_日付で登録
    out = '{}/model/{}'.format(os.path.dirname(os.path.abspath(__file__)), userid)
    dir_exist(out)
    model.save('{}/{}_{}.h5'.format(out, userid, datetime.datetime.today().strftime("%Y-%m-%d-%H-%M-%S")))
    open('{}/{}_{}_model.json'.format(out, userid, datetime.datetime.today().strftime("%Y-%m-%d-%H-%M-%S")), 'w').write(model.to_json())
    keras.backend.clear_session()
    os.remove('./logs/loss.csv')
    

def select_pretrained(args):
    if args.pretrained == 'vgg16':
        return VGG16(weights='imagenet',include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'vgg19':
        return VGG19(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'xception':
        return Xception(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'resnet50':
        return ResNet50(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'inceptionv3':
        return InceptionV3(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'densenet201':
        return DenseNet201(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'densenet169':
        return DenseNet169(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'densenet121':
        return DenseNet121(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'inceptionresnetv2':
        return InceptionResNetV2(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'nasnetmobile':
        return NASNetMobile(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'nasnetlarge':
        return NASNetLarge(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'mobilenet':
        return MobileNet(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    elif args.pretrained == 'mobilenetv2':
        return MobileNetV2(weights='imagenet', include_top=False, input_tensor=Input((224,224,3)))
    

def select_optimizer(args):
    if args.optimizer == 'SGD':
        return SGD(lr=args.lr, momentum=0.09)
    elif args.optimizer == 'RMSprop':
        return RMSprop(lr=args.lr, rho=0.9, epsilon=None, decay=0.0)
    elif args.optimizer == 'Adagrad':
        return Adagrad(lr=args.lr, epsilon=None, decay=0.0)
    elif args.optimizer == 'Adadelta':
        return Adadelta(lr=args.lr, rho=0.95, epsilon=None, decay=0.0)
    elif args.optimizer == 'Adam':
        return Adam(lr=args.lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    elif args.optimizer == 'Adamax':
        return Adamax(lr=args.lr, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)
    elif args.optimizer == 'Nadam':
        return Nadam(lr=args.lr, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) 

'''
if __name__ == '__main__':
    params = setup(1, 10, False, False, False, False, False, 0.0001, 'binary_crossentropy', 'xception', 'Nadam', 'test')
    do_train('test', params)
    '''