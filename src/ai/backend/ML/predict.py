from keras.models import load_model
import numpy as np
from keras.preprocessing.image import img_to_array, load_img
import glob
import json

def classification(model_path, img_path):
    model = load_model(model_path)
    img = img_to_array(load_img(img_path, target_size=(224,224)))
    img_nad = img_to_array(img)/255
    img_nad = img_nad[None, ...]
    filelist = glob.glob('./*label*')
    f = open(filelist[0], 'r')
    label = json.load(f)
    print(label)
    pred = model.predict(img_nad, batch_size=1, verbose=1)
    score = np.max(pred)
    pred_label = label[np.argmax(pred[0])]
    return score, pred_label
    # print('name:',pred_label)
    # print('score:',score)

'''
if __name__ == '__main__':
    classification('./test_2019-12-26-21-38-55.h5', './datasets/test/train/cat/cat3.jpeg')
    '''
    