import Vue from 'vue'
import VueRouter from 'vue-router'
import root from '@/frontend/views/root'
import signup from '@/frontend/views/signup'
import dataset from '@/frontend/views/logged/dataset'
import train from '@/frontend/views/logged/train'
import prediction from '@/frontend/views/logged/prediction'
import weightoperation from '@/frontend/views/logged/weightoperation'
import info from '@/frontend/views/logged/info'
import confirm from '@/frontend/views/confirm'
import monitor from '@/frontend/views/logged/monitor'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: root,
    name: 'root',
    meta: {
      isPublic: true
    }
  },
  {
    path: '/signup',
    component: signup,
    name: 'signup',
    meta: {
      isPublic: true
    }
  },
  {
    path: '/confirm',
    component: confirm,
    name: 'confirm',
    meta: {
      isPublic: true
    }
  },
  {
    path: '/logged/dataset',
    component: dataset,
    name: 'dataset'
  },
  {
    path: '/logged/train',
    component: train,
    name: 'train'
  },
  {
    path: '/logged/prediction',
    component: prediction,
    name: 'prediction'
  },
  {
    path: '/logged/weightoperation',
    component: weightoperation,
    name: 'weightoperation'
  },
  {
    path: '/logged/info',
    component: info,
    name: 'info'
  },
  {
    path: '/logged/monitor',
    component: monitor,
    name: 'monitor'
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
