import Vue from 'vue'

import '@/styles/quasar.sass'
import '@quasar/extras/material-icons/material-icons.css'
import { Quasar, QIcon, QCarousel, QCarouselControl, QCarouselSlide, Dialog, Loading } from 'quasar'


Vue.use(Quasar, {
  config: {},
  components: { 
    QIcon,
    QCarousel,
    QCarouselControl,
    QCarouselSlide
  },
  directives: { /* not needed if importStrategy is not 'manual' */ },
  plugins: {
    Dialog,
    Loading
  },
  extras: [
    'material-icons',
    'material-icons-outlined',
    'material-icons-round',
    'material-icons-sharp',
    'mdi-v4',
    'ionicons-v4',
    'eva-icons',
    'fontawesome-v5',
    'themify'
  ]
 })