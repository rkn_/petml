import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './quasar'
import VueWebsocket from "vue-websocket"
// import { post } from '@/frontend/libs/post'
import VueApexCharts from 'vue-apexcharts'

Vue.component('apexchart', VueApexCharts)
Vue.use(VueWebsocket, 'ws://localhost:8000/api/getloss')

Vue.config.productionTip = false

// front 開発はコメントアウト
/*
router.beforeEach((to, _, next) => {
  let param = new URLSearchParams()
  let state = store.getters.getState
  // 公開されていないページへのアクセス
  if (to.matched.some(record => !record.meta.isPublic)) {
    param.append("jwt", state.auth.token)
    // token の有効期限を確認
    post('/api/tokenValidation', param).then(res => {
      if(res.data) {
        next()
      }else{
        next('/signup')
      }
    })
  } else {
    next()
  }
})
*/



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
