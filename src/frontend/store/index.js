import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    auth: {
      login: false,
      token: ''
    },
    training: false
  },
  mutations: {
    login(state, token) {
      state.auth.token = token
      state.auth.login = true
    },
    logout(state) {
      state.auth.token = ''
      state.auth.login = false
    },
    startTrain(state) {
      state.training = true
    },
    endTrain(state) {
      state.training = false
    }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    getState(state) {
      return state
    },
    getTrain(state){
      return state.training
    }
  },
  plugins: [createPersistedState()]
})
