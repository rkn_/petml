import store from '@/frontend/store'
import router from '@/frontend/router'
export function login(data){
  if(data.Login){
    store.commit('login', data.Token)
    router.push('/')
  }else{
    // error code に応じて dialog を出力
    router.push('/signup')
  }
}
export function signup(data) {
  console.log(data)
  if(data.next) {
    let id = encode(data.id)
    let mail = encode(data.mail)
    router.push({ path: '/confirm', query: { id: id, addr: mail } })
  }else{
    // error codeに応じて dialog を出力
    router.push('/signup')
  }
}

function encode(str) {
  return btoa(unescape(encodeURIComponent(str)))
}

export function auth() {

}
