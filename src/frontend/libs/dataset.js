export function parsetree (data) {
	let tree = []
	tree.push({ label: '', children: [] })
  for(let key in data) {
		let obj = {
			label: key,
			children: []
		}
		for(let i = 0;i < data[key].length; i++){
			obj.children.push({label: data[key][i].Label, file: data[key][i].File, basekey: data[key][i].BaseKey})
		}
		tree[0].children.push(obj)
	}
  return tree
}