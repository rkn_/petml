package utils

import (
	"github.com/gin-gonic/contrib/sessions"
)

// StoreUserID store user id when authorized
func StoreUserID(userid string, session sessions.Session) {
	session.Set("userid", userid)
	session.Save()
}
