package utils

import (
	"io/ioutil"
	"os"
	"time"

	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
)

// CreateToken create jwt token
func CreateToken() string {
	gopath := os.Getenv("GOPATH")
	base := gopath + "/src/petml/src/backend/ssl/"
	privepath := base + "priv.key"
	bytes, _ := ioutil.ReadFile(privepath)
	claims := jws.Claims{}
	claims.SetExpiration(time.Now().Add(time.Duration(12) * time.Hour))

	rsaPrivate, _ := crypto.ParseRSAPrivateKeyFromPEM(bytes)
	jwt := jws.NewJWT(claims, crypto.SigningMethodRS256)

	token, _ := jwt.Serialize(rsaPrivate)
	return string(token)
}

// Validation validate token is true or false
func Validation(token string) bool {
	gopath := os.Getenv("GOPATH")
	base := gopath + "/src/petml/src/backend/ssl/"
	keyPath := base + "pub.key"
	bytes, _ := ioutil.ReadFile(keyPath)
	rsaPublic, _ := crypto.ParseRSAPublicKeyFromPEM(bytes)

	jwt, err := jws.ParseJWT([]byte(token))
	if err != nil {
		return false
	}

	// Validate token
	if err = jwt.Validate(rsaPublic, crypto.SigningMethodRS256); err != nil {
		return false
	}
	return true
}
