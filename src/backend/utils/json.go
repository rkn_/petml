package utils

import (
	"encoding/json"
	"strings"
)

// CreateJSON create json string
func CreateJSON(obj interface{}, small bool) string {
	bytes, _ := json.Marshal(obj)
	jsonstr := string(bytes)
	if small {
		jsonstr = strings.ToLower(jsonstr)
	}
	return jsonstr
}
