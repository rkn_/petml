package utils

import (
	"mime/multipart"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
)

// F is a datastructure
type F struct {
	Name string
	Ext  string
	Data multipart.File
}

// GetFiles is called when accept multiple files request
func GetFiles(req *http.Request) []F {
	exist := true
	cnt := 1
	var files []F
	for exist {
		var tmp F
		file, header, _ := req.FormFile("file" + strconv.Itoa(cnt))
		if file != nil {
			tmp.Data = file
			tmp.Ext = filepath.Ext(header.Filename)
			tmp.Name = strings.Replace(header.Filename, tmp.Ext, "", -1)
			files = append(files, tmp)
		} else {
			exist = false
		}
		cnt++
	}
	return files
}
