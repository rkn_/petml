package utils

import (
	"fmt"
	lib "petml/src/backend/lib/connection"
)

// Exec run lock db
func Exec(query string, args []interface{}) bool {
	db := lib.Connect()
	defer db.Close()

	tx, _ := db.Begin()
	_, err := tx.Exec(query, args...)
	success := false
	if err != nil {
		tx.Rollback()
		fmt.Println(err)
	} else {
		tx.Commit()
		success = true
	}
	return success
}

// GetRow run lock db
func GetRow(query string, args []interface{}) [][]interface{} {
	db := lib.Connect()
	defer db.Close()
	tx, _ := db.Begin()
	rows, err := tx.Query(query, args...)
	col, _ := rows.Columns()
	colnum := len(col)
	var res [][]interface{}
	for rows.Next() {
		elm := make([]interface{}, colnum)
		ptr := make([]interface{}, colnum)
		for i := range col {
			ptr[i] = &elm[i]
		}
		rows.Scan(ptr...)
		res = append(res, elm)
	}
	defer tx.Commit()
	if err != nil {
		tx.Rollback()
		return nil
	}
	return res
}
