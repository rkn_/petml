package router

import (
	"fmt"
	"os"
	"petml/src/backend/api"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// CreateRouter create routing this application.
func CreateRouter(docker bool) *gin.Engine {
	router := gin.Default()

	store := sessions.NewCookieStore([]byte("secret"))
	router.Use(sessions.Sessions("session", store))
	gopath := os.Getenv("GOPATH")

	base := gopath + "/src/petml/dist"
	fmt.Println(docker)
	if docker {
		base = gopath + "/src/petml/src/backend/static"
	}
	router.LoadHTMLFiles(base + "/index.html")
	router.Static("/css", base+"/css")
	router.Static("/js", base+"/js")
	router.Static("/fonts", base+"/fonts")
	router.Static("/favicon.ico", base+"/favicon.ico")
	router.GET("/", view)

	apigroup := router.Group("/api")
	{
		apigroup.POST("/login", api.Login)
		apigroup.POST("/signup", api.Signup)
		apigroup.POST("/confirm", api.Confirm)
		apigroup.POST("/logout", api.Logout)
		apigroup.POST("/tokenValidation", api.TokenValidate)

		apigroup.POST("/addclass", api.AddClass)
		apigroup.POST("/getclasses", api.GetClasses)
		apigroup.POST("/delclass", api.DelClass)
		apigroup.POST("/getdataset", api.GetDataset)
		apigroup.POST("/createdataset", api.CreateDataset)
		apigroup.POST("/deletedataset", api.DeleteDataset)
		apigroup.POST("/upload", api.Uploaddata)
		apigroup.POST("/getbasedata", api.GetData)
		apigroup.POST("/deletedata", api.DeleteData)

		apigroup.POST("/storeTrain", api.StoreTrain)

		apigroup.POST("/getdatasetname", api.GetDatasetName)
	}
	service := router.Group("/service")
	{
		service.POST("/getinfo", api.Getinfo)
		service.POST("/update", api.Update)
	}
	return router
}

func view(g *gin.Context) {
	g.HTML(200, "index.html", gin.H{})
}
