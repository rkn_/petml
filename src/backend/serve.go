package main

import (
	"flag"
	"petml/src/backend/router"
)

func main() {
	var (
		docker = flag.Bool("docker", false, "use docker ?")
	)
	flag.Parse()
	// docker で実行するときはtrue
	r := router.CreateRouter(*docker)
	r.Run(":8080")
}
