package lib

import (
	"database/sql"
	"fmt"

	// need connect postgres
	_ "github.com/lib/pq"
)

// Connect method connect db
func Connect() *sql.DB {
	// local
	db, err := sql.Open("postgres", "host=petml_db port=5432 user=lkeix password=dummy dbname=petml sslmode=disable")
	if err != nil {
		panic(err)
	}
	fmt.Println(db)
	return db
}
