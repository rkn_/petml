package lib

import (
	"bytes"
	"encoding/base64"
	"errors"
	"io"
	"mime/multipart"
	"petml/src/backend/utils"
	"regexp"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// Data is a datastruecture
type Data struct {
	Label   string
	File    string
	BaseKey string
}

// Dataset is a datastructure
type Dataset struct {
	Classes map[string][]Data
}

func createsess(config *utils.Config) *session.Session {
	sess := session.Must(session.NewSession(&aws.Config{
		Credentials: credentials.NewStaticCredentials(config.Aws.S3.AccessKeyID, config.Aws.S3.SecretAccessKey, ""),
		Region:      aws.String(config.Aws.S3.Region),
	}))
	return sess
}

// GetData get S3 dataset
func GetData(userid, did string) (string, error) {
	config := utils.NewConfig()
	sess := createsess(config)
	svc := s3.New(sess)
	res, err := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(config.Aws.S3.Bucket)})
	if err != nil {
		return "", err
	}
	mp := make(map[string][]Data)
	// userid/データセットID/class名/画像名
	for _, item := range res.Contents {
		basekey := *item.Key
		key := strings.Replace(basekey, " ", "", -1)
		key = strings.Replace(basekey, "/", " ", -1)
		reg := regexp.MustCompile(`\S+`)
		elm := reg.FindAllString(key, -1)
		ID := elm[0]
		datasetid := elm[1]
		class := elm[2]
		imgname := elm[3]
		if ID == userid && datasetid == did {
			var data Data
			data.Label = imgname
			data.BaseKey = basekey
			res, err := svc.GetObject(&s3.GetObjectInput{
				Bucket: aws.String(config.Aws.S3.Bucket),
				Key:    aws.String(basekey),
			})
			if err != nil {
				panic(err)
			}
			buf := new(bytes.Buffer)
			io.Copy(buf, res.Body)
			data.File = base64.StdEncoding.EncodeToString(buf.Bytes())
			mp[class] = append(mp[class], data)
		}
	}
	jsonstr := utils.CreateJSON(mp, false)
	return jsonstr, nil
}

// Upload is S3 upload functon
func Upload(file multipart.File, filename, ext, dist string) error {
	if checkname(filename) {
		return errors.New("file name is required")
	}
	format, exterr := checkformat(ext)
	if exterr != nil {
		return exterr
	}
	config := utils.NewConfig()
	sess := createsess(config)
	uploader := s3manager.NewUploader(sess)
	// upload s3
	_, err := uploader.Upload(&s3manager.UploadInput{
		Body:        file,
		Bucket:      aws.String(config.Aws.S3.Bucket),
		ContentType: aws.String(format),
		Key:         aws.String(dist),
	})
	if err != nil {
		return err
	}
	return nil
}

func checkname(name string) bool {
	if name != "" {
		return false
	}
	return true
}

func checkformat(ext string) (string, error) {
	ext = ext[1:]
	var contentType string
	switch ext {
	case "jpg":
		contentType = "image/jpeg"
	case "jpeg":
		contentType = "image/jpeg"
	case "gif":
		contentType = "image/gif"
	case "png":
		contentType = "image/png"
	default:
		return "", errors.New("this extension is invalid")
	}
	return contentType, nil
}

// DeleteData delete execution
func DeleteData(key string) bool {
	config := utils.NewConfig()
	sess := createsess(config)
	svc := s3.New(sess)
	_, err := svc.DeleteObject(&s3.DeleteObjectInput{Bucket: aws.String(config.Aws.S3.Bucket), Key: aws.String(key)})
	if err != nil {
		return false
	}
	err = svc.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: aws.String(config.Aws.S3.Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return false
	}
	return true
}

// DeleteDatas delete folder
func DeleteDatas(userid, did string) {
	config := utils.NewConfig()
	sess := createsess(config)
	svc := s3.New(sess)
	res, _ := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(config.Aws.S3.Bucket)})
	// userid/データセットID/class名/画像名
	for _, item := range res.Contents {
		basekey := *item.Key
		key := strings.Replace(basekey, " ", "", -1)
		key = strings.Replace(basekey, "/", " ", -1)
		reg := regexp.MustCompile(`\S+`)
		elm := reg.FindAllString(key, -1)
		ID := elm[0]
		datasetid := elm[1]
		if ID == userid && datasetid == did {
			DeleteData(basekey)
		}
	}
}
