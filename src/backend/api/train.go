package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"petml/src/backend/utils"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Datasets is a datastructure
type Datasets struct {
	Name string
	ID   int64
}

// Train is a datastructure
type Train struct {
	Trainid string
	Options string
}

// GetDatasetName is called when get options
func GetDatasetName(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	datas := getdatasetname(userid)
	jsonstr := utils.CreateJSON(datas, true)
	ctx.Writer.WriteString(jsonstr)
}

func getdatasetname(userid string) []Datasets {
	query := "select datasetid, name from dataset where userid=$1"
	args := []interface{}{
		userid,
	}
	var res []Datasets
	datasetids := utils.GetRow(query, args)
	for _, val := range datasetids {
		var tmp Datasets
		tmp.ID = val[0].(int64)
		tmp.Name = val[1].(string)
		res = append(res, tmp)
	}
	return res
}

// StoreTrain store train id and options
func StoreTrain(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	jsonstr := req.PostFormValue("data")
	trainid := storetrain(userid, jsonstr)
	var train Train
	train.Trainid = trainid
	train.Options = jsonstr
	jsonStr, _ := json.Marshal(train)
	req, err := http.NewRequest(
		"POST",
		"http://ml/",
		bytes.NewBuffer(jsonStr),
	)
	fmt.Println(req)
	if err != nil {
		panic(err)
	}
}

func storetrain(userid, option string) string {
	trainid := utils.GenerateRandStr(32)
	query := "insert into train (trainid, options, userid) values ($1, $2, $3)"
	args := []interface{}{
		trainid, option, userid,
	}
	for !utils.Exec(query, args) {
		trainid = utils.GenerateRandStr(32)
		args = []interface{}{
			trainid, option, userid,
		}
	}
	return trainid
}
