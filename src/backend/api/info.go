package api

import (
	"petml/src/backend/utils"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Info is a data structure
type Info struct {
	UserID string
	Mail   string
}

// Getinfo get user information
func Getinfo(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	query := "select userid, mail from users where userid = $1"
	args := []interface{}{
		userid,
	}
	data := utils.GetRow(query, args)
	var res Info
	res.UserID = data[0][0].(string)
	res.Mail = data[0][1].(string)
	jsonstr := utils.CreateJSON(res, true)
	ctx.Writer.Write(([]byte)(jsonstr))
}

// Update is called by userinfo update
func Update(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	id := req.PostFormValue("ID")
	Password := req.PostFormValue("Password")
	Mail := req.PostFormValue("mail")
	msg := ""
	if check(userid, id, Mail) == 0 {
		if update(userid, id, Mail, Password) {
			msg = "正常に更新しました。"
			session.Set("userid", id)
			session.Save()
		} else {
			msg = "更新中にエラーが発生しました。"
		}
	} else if check(userid, id, Mail) == 1 {
		msg = "既に登録されているIDです。"
	} else if check(userid, id, Mail) == 2 {
		msg = "既に登録されているメールアドレスです。"
	}
	ctx.Writer.WriteString(msg)
}

func check(currentid, id, mail string) int {
	query := "select userid from users where userid = $1"
	args := []interface{}{
		id,
	}
	res := utils.GetRow(query, args)
	compare := res[0][0].(string)
	if len(res) > 0 && compare != currentid {
		return 1
	}
	query = "select userid from users where mail = $1"
	args = []interface{}{
		mail,
	}
	res = utils.GetRow(query, args)
	compare = res[0][0].(string)
	if len(res) > 0 && compare != currentid {
		return 2
	}
	return 0
}

func update(currentid, id, mail, password string) bool {
	hashed := utils.CryptPasswd(password)
	query := ""
	var args []interface{}
	if len(password) < 8 {
		query = "update users set userid = $1, mail = $2 where userid = $3"
		args = []interface{}{
			id, mail, currentid,
		}
	} else {
		query = "update users set userid = $1, password = $2, mail = $3 where userid = $4"
		args = []interface{}{
			id, hashed, mail, currentid,
		}
	}

	return utils.Exec(query, args)
}
