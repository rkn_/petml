package api

import (
	"encoding/json"
	"petml/src/backend/utils"
	"strings"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Res is a response data structure
type Res struct {
	Login bool
	Token string
}

// SignRes is a response data structure
type SignRes struct {
	Next bool
	Mail string
	ID   string
}

// ConfirmRes is a response data structure
type ConfirmRes struct {
	Next  bool
	Token string
}

// Signup run when user signup
func Signup(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	ID := req.PostFormValue("ID")
	mail := req.PostFormValue("Mail")
	Password := req.PostFormValue("Password")
	jsonstr := signup(ID, Password, mail)
	ctx.Writer.Write(([]byte)(jsonstr))
}

// Login run when user login authorization
func Login(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	ID := req.PostFormValue("ID")
	Password := req.PostFormValue("Password")
	var obj Res
	if auth(ID, Password) {
		obj.Login = true
		obj.Token = utils.CreateToken()
		session := sessions.Default(ctx)
		utils.StoreUserID(ID, session)
	} else {
		obj.Login = false
		obj.Token = ""
	}
	jsonstr := createjson(obj, false)
	ctx.Writer.Write(([]byte)(jsonstr))
}

// Confirm run when confirm passcode
func Confirm(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	ID := req.PostFormValue("ID")
	passcode := req.PostFormValue("passcode")
	isok, token := confirm(ID, passcode)
	var res ConfirmRes
	if isok {
		res.Next = true
		res.Token = token
		session := sessions.Default(ctx)
		utils.StoreUserID(ID, session)
	} else {
		res.Next = false
		res.Token = ""
	}
	jsonstr := createjson(res, false)
	ctx.Writer.Write(([]byte)(jsonstr))
}

// Logout is called when logout process
func Logout(ctx *gin.Context) {
	session := sessions.Default(ctx)
	session.Clear()
	session.Save()
}

// TokenValidate validate client's token is avalable or not
func TokenValidate(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	token := req.PostFormValue("jwt")
	if utils.Validation(token) {
		ctx.Writer.WriteString("true")
	} else {
		ctx.Writer.WriteString("false")
	}
}

func confirm(ID, passcode string) (bool, string) {
	query := "select passcode from confirm where userid = $1"
	args := []interface{}{
		ID,
	}
	res := utils.GetRow(query, args)
	var ps string
	if len(res) > 0 {
		ps = res[0][0].(string)
	}
	if ps == passcode {
		token := utils.CreateToken()
		query := "update users set active = true where userid = $1"
		utils.Exec(query, args)
		query = "delete from confirm where userid = $1"
		utils.Exec(query, args)
		return true, token
	}
	return false, ""
}

func signup(ID, password, mail string) string {
	var signres SignRes
	signres.Next = false
	password = utils.CryptPasswd(password)
	args := []interface{}{
		ID, password, mail, false,
	}
	query := "insert into users (userid, password, mail, active) values ($1, $2, $3, $4)"
	if utils.Exec(query, args) {
		signres.ID = ID
		signres.Mail = mail
		signres.Next = true
	} else {
		jsonstr := createjson(signres, true)
		return jsonstr
	}
	passcode := utils.GenerateOneTime()
	args = []interface{}{
		ID, passcode,
	}
	query = "insert into confirm (userid, passcode) values ($1, $2)"
	jsonstr := createjson(signres, true)
	utils.Exec(query, args)
	return jsonstr
}

func auth(id, ps string) bool {
	query := "select password, active from users where userid = $1"
	args := []interface{}{
		id,
	}
	res := utils.GetRow(query, args)
	var hashed string
	var active bool
	if len(res) > 0 {
		hashed = res[0][0].(string)
		active = res[0][1].(bool)
	}
	if utils.ComparePasswd(ps, hashed) && active {
		return true
	}
	return false
}

func createjson(obj interface{}, small bool) string {
	bytes, _ := json.Marshal(obj)
	jsonstr := string(bytes)
	if small {
		jsonstr = strings.ToLower(jsonstr)
	}

	return jsonstr
}
