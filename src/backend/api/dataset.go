package api

import (
	"encoding/json"
	"petml/src/backend/lib"
	"petml/src/backend/utils"
	"strconv"
	"strings"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Class is a datastructure
type Class struct {
	Label string
	ID    int64
}

// Dataset is a datastructure
type Dataset struct {
	Name  string
	ID    int64
	Image string
}

// Upload is a datastructure
type Upload struct {
	Dataset string
	Class   string
}

// GetClasses is called by client when confirm classes
func GetClasses(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	jsonstr := getclass(userid)
	ctx.Writer.WriteString(jsonstr)
}

func getclass(userid string) string {
	query := "select class.name, class.classid from class, relation where relation.userid = $1 and relation.classid = class.classid"
	args := []interface{}{
		userid,
	}
	res := utils.GetRow(query, args)
	var classes []Class
	for _, val := range res {
		var class Class
		class.Label = val[0].(string)
		class.ID = val[1].(int64)
		classes = append(classes, class)
	}
	jsonstr := utils.CreateJSON(classes, true)
	return jsonstr
}

// AddClass add new class
func AddClass(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	jsonstr := req.PostFormValue("data")
	var data []string
	json.Unmarshal(([]byte)(jsonstr), &data)
	for _, val := range data {
		addclass(userid, val)
	}
}

func addclass(userid, class string) {
	query := "insert into class (name) values ($1) RETURNING classid"
	args := []interface{}{
		class,
	}
	res := utils.GetRow(query, args)
	classid := res[0][0].(int64)
	query = "insert into relation (userid, classid) values ($1, $2)"
	args = []interface{}{
		userid, classid,
	}
	utils.Exec(query, args)
}

// DelClass is called by client when client delete classes
func DelClass(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	var data []int64
	jsonstr := req.PostFormValue("data")
	json.Unmarshal(([]byte)(jsonstr), &data)
	query := ""
	for _, val := range data {
		query = "delete from relation where userid = $1 and classid = $2"
		args := []interface{}{
			userid, val,
		}
		utils.Exec(query, args)
		query = "delete from class where classid = $1"
		args = []interface{}{
			val,
		}
		utils.Exec(query, args)
	}
}

// CreateDataset create dataset
func CreateDataset(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	datasetname := req.PostFormValue("name")
	id := strconv.FormatInt(createdataset(userid, datasetname), 10)
	ctx.Writer.WriteString(id)
}

func createdataset(userid, datasetname string) int64 {
	query := "insert into dataset (name, userid) values ($1, $2) RETURNING datasetid"
	args := []interface{}{
		datasetname, userid,
	}
	res := utils.GetRow(query, args)
	id := res[0][0].(int64)
	return id
}

// GetDataset get dataset id, name
func GetDataset(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	datasets := getdataset(userid)
	jsonstr := utils.CreateJSON(datasets, false)
	ctx.Writer.WriteString(jsonstr)
}
func getImageLink(userid, datasetname string) string {
	return "https://cdn.quasar.dev/img/mountains.jpg"
}

func getdataset(userid string) []Dataset {
	query := "select datasetid, name from dataset where userid=$1"
	args := []interface{}{
		userid,
	}
	var datasets []Dataset
	res := utils.GetRow(query, args)
	for _, val := range res {
		var dataset Dataset
		dataset.ID = val[0].(int64)
		dataset.Name = val[1].(string)
		dataset.Image = getImageLink(userid, dataset.Name)
		datasets = append(datasets, dataset)
	}
	return datasets
}

// DeleteDataset delete dataset
func DeleteDataset(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	req.ParseForm()
	id := req.PostFormValue("datasetid")
	deletedataset(id)
	lib.DeleteDatas(userid, id)
}

func deletedataset(id string) {
	query := "delete from dataset where datasetid = $1"
	args := []interface{}{
		id,
	}
	utils.Exec(query, args)
}

// GetData is getdatasets data
func GetData(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	datasetid := req.PostFormValue("datasetid")
	jsonstr, err := lib.GetData(userid, datasetid)
	if err != nil {
		panic(err)
	}
	ctx.Writer.Write(([]byte)(jsonstr))
}

// Uploaddata is called when upload request
func Uploaddata(ctx *gin.Context) {
	session := sessions.Default(ctx)
	userid := session.Get("userid").(string)
	req := ctx.Request
	jsonstr := req.FormValue("data")
	req.ParseForm()
	var base Upload
	json.Unmarshal(([]byte)(jsonstr), &base)
	files := utils.GetFiles(req)
	for _, val := range files {
		dist := userid + "/" + base.Dataset + "/" + base.Class + "/" + val.Name + val.Ext
		dist = strings.Replace(dist, " ", "", -1)
		err := lib.Upload(val.Data, val.Name, val.Ext, dist)
		if err != nil {
			panic(err)
		}
	}
}

// DeleteData is called when dataset's data delete
func DeleteData(ctx *gin.Context) {
	req := ctx.Request
	req.ParseForm()
	jsonstr := req.PostFormValue("keys")
	var keys []string
	json.Unmarshal(([]byte)(jsonstr), &keys)
	ok := true
	for _, key := range keys {
		if !lib.DeleteData(key) {
			ok = false
		}
	}
	if ok {
		ctx.Writer.WriteString("true")
	} else {
		ctx.Writer.WriteString("false")
	}
}
