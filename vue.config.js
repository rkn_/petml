module.exports = {

  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: true
    }
  },
  publicPath: './',
  devServer: {
    port: 5555,
    host: '0.0.0.0',
    disableHostCheck: true,
  },
  pages: {
    top: {
      entry: 'src/frontend/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    }
  },
  transpileDependencies: [
    'quasar'
  ]
}
